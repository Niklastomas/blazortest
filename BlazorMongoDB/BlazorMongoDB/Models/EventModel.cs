﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorMongoDB.Models
{
    public class EventModel
    {
        public string time_stamp { get; set; }
        public string event_type { get; set; }
        public int temperature { get; set; }
     
        public ResultModel[] devices { get; set; }
    }
}

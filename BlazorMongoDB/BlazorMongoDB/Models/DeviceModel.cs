﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorMongoDB.Models
{
    public class DeviceModel
    {
        public string device_id { get; set; }
        public double firmware { get; set; }
        public string label { get; set; }
        public int phase_connection { get; set; }
        public int neutral_connection { get; set; }
        public int mesh_index { get; set; }
        public int serial_number { get; set; }
        public decimal hardware_revision { get; set; }
        public string load { get; set; }
    }
}

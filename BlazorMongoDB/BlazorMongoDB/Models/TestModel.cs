﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorMongoDB.Models
{
    public class TestModel
    {
        [BsonId]
        public Object Id { get; set; }
        public string test_id { get; set; }

        public DeviceModel[] devices { get; set; }
        public EventModel[] events { get; set; }

    }
}

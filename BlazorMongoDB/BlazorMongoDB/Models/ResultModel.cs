﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorMongoDB.Models
{
    public class ResultModel
    {
        public string device_id { get; set; }
        public string result { get; set; }
        public string data { get; set; }
    }
}
